import React, { Component } from 'react'

export default class Criptomoneda extends Component {
  render() {
    const { FullName, Name } = this.props.criptomoneda.CoinInfo

    return <option value={Name}>{FullName}</option>
  }
}
