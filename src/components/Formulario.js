import React, { Component } from 'react'
import axios from 'axios'
import Criptomoneda from './Criptomoneda'
import Error from './Error'

export default class Formulario extends Component {
  state = {
    criptomonedas: [],
    moneda: '',
    criptomoneda: '',
    error: false
  }

  async componentDidMount() {
    const URL =
      'https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD'

    await axios.get(URL).then(respuesta => {
      this.setState({
        criptomonedas: respuesta.data.Data
      })
    })
  }

  obtenerValor = e => {
    const { name, value } = e.target

    this.setState({
      [name]: value
    })
  }

  cotizarModena = e => {
    e.preventDefault()

    const { moneda, criptomoneda } = this.state
    if (moneda === '' || criptomoneda === '') {
      this.setState({ error: true }, () => {
        setTimeout(() => {
          this.setState({ error: false })
        }, 3000)
      })

      return
    }

    const cotizacion = {
      moneda,
      criptomoneda
    }

    this.props.cotizarCriptomoneda(cotizacion)
  }

  render() {
    const mensaje = this.state.error ? (
      <Error mensaje="Ambos campos son obligatorios" />
    ) : (
      ''
    )

    return (
      <div>
        {mensaje}
        <form onSubmit={this.cotizarModena}>
          <div className="row">
            <label>Elige tu Moneda</label>
            <select
              className="u-full-width"
              name="moneda"
              onChange={this.obtenerValor}
            >
              <option value="">Elige tu moneda</option>
              <option value="PEN">Peru</option>
              <option value="USD">Dolar Estadounidense</option>
              <option value="MXN">Peso Mexicano</option>
              <option value="GBP">Libras</option>
              <option value="EUR">Euros</option>
            </select>
          </div>

          <div className="row">
            <label>Elige tu Criptomoneda</label>
            <select
              className="u-full-width"
              name="criptomoneda"
              onChange={this.obtenerValor}
            >
              <option value="">Elige tu moneda</option>
              {Object.keys(this.state.criptomonedas).map(key => (
                <Criptomoneda
                  key={key}
                  criptomoneda={this.state.criptomonedas[key]}
                />
              ))}
            </select>
          </div>
          <input
            className="button-primary u-full-width"
            type="submit"
            value="Cotizar"
          />
        </form>
      </div>
    )
  }
}
