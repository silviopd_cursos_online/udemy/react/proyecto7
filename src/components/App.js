import React, { Component } from 'react'
import '../css/App.css'
import imagen from '../../assests/img/cryptomonedas.png'
import Formulario from './Formulario'
import axios from 'axios'
import Resultado from './Resultado'
import Spinner from './Spinner'

export default class App extends Component {
  state = {
    resultado: {},
    moneda: '',
    criptomoneda: '',
    cargando: false
  }

  cotizarCriptomoneda = async cotizacion => {
    const { moneda, criptomoneda } = cotizacion

    const URL = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${criptomoneda}&tsyms=${moneda}`

    await axios.get(URL).then(resultado => {
      this.setState(
        {
          resultado: resultado.data.DISPLAY[criptomoneda][moneda],
          cargando: true
        },
        () => {
          setTimeout(() => {
            this.setState({
              cargando: false
            })
          }, 3000)
        }
      )
    })
  }

  render() {
    const resultado = this.state.cargando ? (
      <Spinner />
    ) : (
      <Resultado resultado={this.state.resultado} />
    )

    return (
      <div className="App">
        <div className="container">
          <div className="row">
            <div className="one-half column">
              <img src={imagen} alt="imagen" className="logotipo" />
            </div>
            <div className="one-half column">
              <h1>Cotiza criptomonedas al instante</h1>
              <Formulario cotizarCriptomoneda={this.cotizarCriptomoneda} />
              {resultado}
            </div>
          </div>
        </div>
      </div>
    )
  }
}
