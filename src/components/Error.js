import React, { Component } from 'react'

export default class Error extends Component {
  render() {
    return (
      <div>
        <p className="error">{this.props.mensaje}</p>
      </div>
    )
  }
}
