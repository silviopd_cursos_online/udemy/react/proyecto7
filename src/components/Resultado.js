import React, { Component } from 'react'

export default class Resultado extends Component {
  render() {
    if (Object.entries(this.props.resultado).length === 0) return null

    const {
      PRICE,
      HIGHDAY,
      LOWDAY,
      CHANGEPCT24HOUR,
      LASTUPDATE
    } = this.props.resultado
    return (
      <div className="resultado">
        <h2>RESULTADO</h2>
        <p className="precio">
          El precio es: <span>{PRICE}</span>
        </p>
        <p>
          El precio más alto del dia: <span>{HIGHDAY}</span>
        </p>
        <p>
          El precio más bajo del dia: <span>{LOWDAY}</span>
        </p>
        <p>
          Variación últimas 24H: <span>{CHANGEPCT24HOUR}%</span>
        </p>
        <p>
          Última actualización <span>{LASTUPDATE}</span>{' '}
        </p>
      </div>
    )
  }
}
